const assert = require("assert");
const app = require("../../src/app");

describe("'callback' service", () => {
  it("registered the service", () => {
    const service = app.service("callback");

    assert.ok(service, "Registered the service");
  });
});
