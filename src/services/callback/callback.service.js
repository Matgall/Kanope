// Initializes the `callback` service on path `/callback`
const { callback } = require("./callback.class");
const createModel = require("../../models/callback.model");
const hooks = require("./callback.hooks");

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    // paginate: app.get("paginate"),
  };

  // Initialize our service with any options it requires
  app.use("/callback", new callback(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service("callback");

  service.hooks(hooks);

  service.on("created", (cb) => {
    console.log("A new callback as been created");
  });
};
