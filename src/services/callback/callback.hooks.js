const { authenticate } = require("@feathersjs/authentication").hooks;

module.exports = {
  before: {
    all: [],
    find: [authenticate("jwt")],
    get: [],
    create: [
      (context) => {
        let resDevices = [];
        const devicesMac = Object.keys(context.data.devices);

        devicesMac.forEach((mac) => {
          resDevices.push({
            mac: mac,
            rssi: context.data.devices[mac].rssi,
            data: context.data.devices[mac].data,
          });
        });

        context.data.devices = resDevices;
      },
    ],
    update: [],
    patch: [],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
