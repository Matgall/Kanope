const socket = io();

const client = feathers();

client.configure(feathers.socketio(socket));

client.configure(
  feathers.authentication({
    storage: window.localStorage,
  })
);

// Login screen
const loginHTML = `<main class="login container">
  <div class="row">
    <div class="col-12 col-6-tablet push-3-tablet text-center heading">
      <h1 class="font-100">Log in or signup</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-6-tablet push-3-tablet col-4-desktop push-4-desktop">
      <form class="form">
          <input class="block" type="email" name="email" placeholder="email">

          <input class="block" type="password" name="password" placeholder="password">

        <button type="button" id="login" class="button button-primary block signup">
          Log in
        </button>

        <button type="button" id="signup" class="button button-primary block signup">
          Sign up and log in
        </button>
      </form>
    </div>
  </div>
</main>`;

const callbackHTML = `<div class="flex flex-column col col-9">
      <table class="tableau flex flex-column flex-1 clear">
        <thead>
          <tr>
            <th colspan="3">Le Tableau des Callbacks</th>
          </tr>
        </thead>
        <tbody></tbody>
      </table>
    </div>
    <footer class="center-text">
      <a href="#" id="logout" class="button button-primary">
        Sign Out
      </a>
    </footer>`;

// Helper to safely escape HTML
const escape = (str) =>
  str.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");

// Show the login page
const showLogin = (error) => {
  if (document.querySelectorAll(".login").length && error) {
    document
      .querySelector(".heading")
      .insertAdjacentHTML(
        "beforeend",
        `<p>There was an error: ${error.callback}</p>`
      );
  } else {
    document.getElementById("app").innerHTML = loginHTML;
  }
};

// Shows the callback page
const showCallback = async () => {
  document.getElementById("app").innerHTML = callbackHTML;

  // Find the callbacks. They will come with the newest first
  const callbacks = await client.service("callback").find({
    query: {
      $sort: { createdAt: -1 },
    },
  });
  console.log(callbacks);
  callbacks.forEach(addCallback);
};

// Renders a callback to the page
const addCallback = (callback) => {
  const tableau = document.querySelector(".tableau");
  const tableauBody = tableau.querySelector("tbody");
  // Escape HTML to prevent XSS attacks
  const imei = escape(callback.imei);
  const devices = callback.devices;
  let devicesTab = "";
  devices.forEach((device) => {
    const dataToParse = device.data.substring(device.data.length - 16);
    let temp1LM75 = `${
      hexToDec(swapData(dataToParse.substring(0, 4))) / 100
    } °C`;
    let temp2BME = `${
      hexToDec(swapData(dataToParse.substring(4, 8))) / 100
    } °C`;
    let relativeHumidity = `${
      hexToDec(swapData(dataToParse.substring(8, 12))) / 100
    }%`;
    let airPressure = `${hexToDec(
      swapData(dataToParse.substring(12, 16))
    )} hPa`;
    const dataTable = table(
      tr(td(temp1LM75) + td(temp2BME) + td(relativeHumidity) + td(airPressure))
    );
    devicesTab += table(tr(td(device.mac) + td(device.rssi) + td(dataTable)));

    function swapData(data) {
      return data.substring(2, 4) + data.substring(0, 2);
    }

    function hexToDec(hexString) {
      return parseInt(hexString, 16);
    }
  });
  const createdAt = moment(callback.createdAt).format("MMM Do, hh:mm:ss");

  if (tableau) {
    tableauBody.insertAdjacentHTML(
      "afterbegin",
      tr(td(createdAt) + td(imei) + td(devicesTab))
    );
  }

  function table(args) {
    return `<table>${args}</table>`;
  }

  function tr(args) {
    return `<tr>${args}</tr>`;
  }

  function td(args) {
    return `<td>${args}</td>`;
  }
};

// Retrieve email/password object from the login/signup page
const getCredentials = () => {
  const user = {
    email: document.querySelector('[name="email"]').value,
    password: document.querySelector('[name="password"]').value,
  };

  return user;
};

// Log in either using the given email/password or the token from storage
const login = async (credentials) => {
  try {
    if (!credentials) {
      // Try to authenticate using an existing token
      await client.reAuthenticate();
    } else {
      // Otherwise log in with the `local` strategy using the credentials we got
      await client.authenticate({
        strategy: "local",
        ...credentials,
      });
    }

    // If successful, show the tableau page
    showCallback();
  } catch (error) {
    // If we got an error, show the login page
    showLogin(error);
  }
};

const addEventListener = (selector, event, handler) => {
  document.addEventListener(event, async (ev) => {
    if (ev.target.closest(selector)) {
      handler(ev);
    }
  });
};

// "Signup and login" button click handler
addEventListener("#signup", "click", async () => {
  // For signup, create a new user and then log them in
  const credentials = getCredentials();

  // First create the user
  await client.service("users").create(credentials);
  // If successful log them in
  await login(credentials);
});

// "Login" button click handler
addEventListener("#login", "click", async () => {
  const user = getCredentials();

  await login(user);
});

// "Logout" button click handler
addEventListener("#logout", "click", async () => {
  await client.logout();

  document.getElementById("app").innerHTML = loginHTML;
});

client.service("callback").on("created", (cb) => {
  console.log("A new callback as been created", cb);
  addCallback(cb);
});

// Call login right away so we can show the tableau window
// If the user can already be authenticated
login();
