# Kanope

- Mathéo Gallego

## Lancement

1. Assurez-vous d'avoir [NodeJS](https://nodejs.org/) et [npm](https://www.npmjs.com/) installé.
2. Clonez le dépôt
    ```
    git clone git@gitlab.com:Matgall/Kanope.git
    OU
    git clone https://gitlab.com/Matgall/Kanope.git
    ```
3. Installez les packages
    ```
    cd Kanope
    npm install
    ```
4. Lancez l'API de démonstration
5. Lancez l'appli feathers

    ```
    npm start
    ```
    info: Feathers application started on http://localhost:3030

6. Ouvrez un nouveau terminal et placez vous dans le dossier `quasar-project`
    ```
    cd quasar-project
    ```

7. Lancez l'appli Quasar
    ```
    quasar dev
    ```
    App • Opening default browser at http://localhost:8080


## Autres informations

- Je n'ai pas réussi à faire la partie front avec Vue sous Quasar. Mais j'arrive à récupérer les données pré-traîtées que j'affiche dans la console.
- Avec Feathers, j'ai réussi à afficher les données et à faire l'authentification.
- J'ai essayé de vous joindre le .zip en pièce jointe du mail mais le mail a été refusé d'envoi par mesure de "sécurité" ...
